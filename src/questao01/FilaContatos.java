package questao01;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class FilaContatos {
	
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		Queue<Entrevistado> entrevistados = new LinkedList<>();
		operacoes(entrevistados);

	}

	public static void operacoes(Queue<Entrevistado> fila) {
		Integer op=0;
		do {
			System.out.println("1) Inserir Entrevistado 2) Pr�ximo Entrevistado 3)Sair");
			System.out.print("Informe qual opera��o deseja fazer: ");
			op = sc.nextInt();
			sc.nextLine();
			switch (op) {
			case 1: {
				fila.add(addEntrevistado());
				System.out.println("Entrevistado adicionado \n");
				break;
			}
			case 2: {
				proximoEntrevistado(fila);
				break;
			}
			case 3: {
				System.out.println("At� a pr�xima");
				System.exit(0);
				break;
			}
			default:
				System.out.println("Essa op��o n�o existe");
			}
		} while (true);
	}

	public static Entrevistado addEntrevistado() {
		System.out.print("Informe o nome do entrevistado: ");
		String nome = sc.nextLine();
		return new Entrevistado(nome);
	}
	
	public static void proximoEntrevistado(Queue<Entrevistado> fila) {
		Entrevistado obj = fila.poll();
		if (obj != null) {
			System.out.println("\nEntrevistado: "+obj+" foi removido!\n");
		}else {
			System.out.println("\nA lista est� vazia!\n");
			System.out.println("Adicione um contato antes de remover!");
		}
	}
}
