package questao02;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

import questao02.entities.Musica;
import questao02.entities.MusicaInternacional;
import questao02.entities.MusicaNacional;

public class Main {

	private static Scanner sc = new Scanner(System.in);
	private static List<Musica> musicas = new ArrayList<>();
	private static Queue<MusicaNacional> F = new LinkedList<>();
	private static Stack<MusicaInternacional> P = new Stack<>();

	public static void main(String[] args) {
		operacoes();
	}

	public static void operacoes() {
		int opcao;
		do {
			System.out.println("1 - Adicionar m�sicas");
			System.out.println("2 - Listar Fila");
			System.out.println("3 - Listar Pilha");
			System.out.println("4 - Sair");
			System.out.print("Escolha uma op��o: ");
			opcao = sc.nextInt();
			sc.nextLine();
			switch (opcao) {
			case 1:
				organizandoMusicas();
				break;
			case 2:
				listarFila();
				break;
			case 3:
				listarPilha();
				break;
			case 4:
				System.out.println("At� a pr�xima!");
				System.exit(0);
				break;

			default:
				System.out.println("Op��o n�o registrada, tente novamente: ");
				break;
			}
		} while (true);
	}

	private static void organizandoMusicas() {
		char resp='S';
		do {
			System.out.println("Informe o tipo da m�sica: N - Nacional ou I - Internacional");
			String tipo = sc.nextLine().toUpperCase(); 
			addMusica(tipo);
			System.out.println("Quer adicionar mais uma m�sica? S/N");
			resp= sc.nextLine().toUpperCase().charAt(0);
		} while (resp=='S');
		distribuir();
	}

	private static void listarPilha() {
		for (MusicaInternacional musicaIntenacional : P) {
			System.out.println("Musica Internacional: "+ musicaIntenacional.getName());
		}
	}

	private static void listarFila() {
		for (MusicaNacional musicaNacional : F) {
			System.out.println("Musica Nacional: " + musicaNacional.getName());
		}
	}
	
	private static void addMusica(String tipo) {
		System.out.println("Informe o nome da m�sica");
		String nome = sc.nextLine();
		Musica musica;
		if (tipo.equals("N")) {
			musica = new MusicaNacional(nome);
		}else {
			musica = new MusicaInternacional(nome);
		}
		musicas.add(musica);
	}

	private static void distribuir() {
		for (Musica musica : musicas) {
			if (musica instanceof MusicaInternacional) {
				P.add((MusicaInternacional) musica);
			} else if (musica instanceof MusicaNacional){
				F.add((MusicaNacional) musica);
			}
		}
	}

}
