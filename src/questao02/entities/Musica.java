package questao02.entities;

public abstract class Musica {
	
	private String name;
	
	public Musica() {
	}

	public Musica(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public abstract String toString();
	
	
}
