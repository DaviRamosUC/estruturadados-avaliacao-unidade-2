package questao02.entities;

public class MusicaInternacional extends Musica{
	
	public MusicaInternacional(String name) {
		super(name);
	}

	@Override
	public String toString() {
		return "Musica Internacional: " + getName();
	}

	
	
	
}
