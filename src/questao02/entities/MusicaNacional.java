package questao02.entities;

public class MusicaNacional extends Musica{
	
	public MusicaNacional(String name) {
		super(name);
	}

	@Override
	public String toString() {
		return "Musica Nacional: " + getName();
	}

	
	
	
}
