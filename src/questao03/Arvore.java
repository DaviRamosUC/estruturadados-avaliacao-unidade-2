package questao03;

class Nodo {
	public int item;
	public Nodo dir;
	public Nodo esq;
}

public class Arvore {
	private Nodo raiz;

	public Arvore() {
		raiz = null;
	}

	public void inserir(Integer valor) {
		Nodo aux = new Nodo();
		aux.item = valor;
		aux.dir = null;
		aux.esq = null;

		if (raiz == null)
			raiz = aux;
		else {
			Nodo atual = raiz;
			Nodo anterior;
			while (true) {
				anterior = atual;
				if (valor <= atual.item) {
					atual = atual.esq;
					if (atual == null) {
						anterior.esq = aux;
						return;
					}
				} else {
					atual = atual.dir;
					if (atual == null) {
						anterior.dir = aux;
						return;
					}
				}
			} // fim do while
		}
	} // fim do inserir

	public static void preOrder(Nodo atual) {
		if (atual != null) {
			System.out.print(atual.item + " ");
			preOrder(atual.esq);
			preOrder(atual.dir);
		}
	}

	public void exibirArvore() {
		preOrder(raiz);
	}
}
