package questao03;

import java.util.Scanner;

public class Main {

	public static Scanner sc = new Scanner(System.in);

	@SuppressWarnings("null")
	public static void main(String[] args) {

		Arvore raiz = new Arvore();
		int opcao;

		do {
			System.out.println("\nOpera��es com uma �rvore");
			System.out.println("1- Preencher a �rvore");
			System.out.println("2- Consultar a �rvore em pr�-ordem");
			System.out.println("3 - sair");
			System.out.print("Escolha uma op��o: ");
			opcao = sc.nextInt();

			switch (opcao) {
			case 1: {
				for (int i = 0; i < 10; i++) {
					System.out.println("Informe o n�mero a ser inserido: ");
					int numero = sc.nextInt();
					if (numero % 2 == 0) {
						raiz.inserir(numero);
					}else {
						System.out.println("O valor n�o � par, tente novamente");
						i--;
					}
				}
				System.out.println("N�meros cadastrados com sucesso!");
				break;
			}
			case 2: {
				if (raiz == null) {
					System.out.println("Sua �rvore est� vazia, utilize a op��o 1 para preencher");
				} else {
					System.out.println("Os n�meros da �rvore s�o: ");
					raiz.exibirArvore();
				}

				break;
			}
			case 3: {
				System.out.println("At� a pr�xima!");
				System.exit(0);
				break;
			}

			}
		} while (true);

	}
}
